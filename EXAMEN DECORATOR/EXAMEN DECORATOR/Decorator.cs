﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN_DECORATOR
{
    // La clase se hereda de complemento dona 
    public abstract class Decorator : ComplementoDona
    {
        //Se establece de tipo  protected para ser empleada por Decorator (Aqui se implementa el patron decorator)
        protected ComplementoDona _dona;
        // La instancia de dona es el plus que se relaciona con la interfaz ComplementoDona (Patrón de diseño DECORATOR)
        public Decorator(ComplementoDona dona)
        {
            _dona = dona;
        }
    }
}
