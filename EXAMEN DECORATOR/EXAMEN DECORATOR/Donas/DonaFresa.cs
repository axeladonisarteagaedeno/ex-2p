﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN_DECORATOR
{
    public class DonaFresa : ComplementoDona
    {
        // Se realiza una operación abstracta de solo lectura que indique la descripción de la dona base a decorar
        public override string Descripcion => "Dona Fresa"; //Descripcion particular de la Dona
    }
}
