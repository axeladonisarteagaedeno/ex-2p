﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace EXAMEN_DECORATOR
{
    public class DonaChocolate : ComplementoDona
    {
        public override string Descripcion => "Dona Chocolate"; //Descripcion particular de la Dona
    }
}
