﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace EXAMEN_DECORATOR
{
    public class DonaVainilla : ComplementoDona
    {
        public override string Descripcion => "Dona Vainilla"; //Descripcion particular de la Dona
    }
}
