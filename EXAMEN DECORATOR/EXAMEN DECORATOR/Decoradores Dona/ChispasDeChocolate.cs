﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace EXAMEN_DECORATOR
{
    public class ChispasDeChocolate : Decorator
    {
        public ChispasDeChocolate(ComplementoDona dona) : base(dona) { } // Se establece el objeto al que se le agregará Chispas de Chocolate, en este caso es una dona

        public override string Descripcion => string.Format($"{_dona.Descripcion}, Chispas de Chocolate"); //Se agrega la descripción que identifica a los decoradores de una dona
    }
}
