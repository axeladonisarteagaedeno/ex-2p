﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN_DECORATOR
{
    public class FrutasConfitadas : Decorator 
    {
        public FrutasConfitadas(ComplementoDona dona) : base(dona) { } // Se establece el objeto al que se le agregará Frutas Confitadas, en este caso es una dona

        public override string Descripcion => string.Format($"{_dona.Descripcion}, Frutas Confitadas"); //Se agrega la descripción que identifica a los decoradores de una dona
    }
}
