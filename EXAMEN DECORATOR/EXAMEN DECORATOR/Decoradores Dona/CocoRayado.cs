﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN_DECORATOR
{
    public class CocoRayado : Decorator
    {
        public CocoRayado(ComplementoDona dona) : base(dona) { } // Se establece el objeto al que se le agregará Coco Rayado, en este caso es una dona

        public override string Descripcion => string.Format($"{_dona.Descripcion}, Coco Rayado"); //Se agrega la descripción que identifica a los decoradores de una dona
    }
}
