﻿using System;

namespace EXAMEN_DECORATOR
{
    class Program
    {
        static void Main(string[] args)
        {
            ComplementoDona dona1 = new DonaChocolate(); //Se crea un pedido nuevo de una dona (Se permite elegir la dona base a gusto)

            //Se agregan los decoradores a la dona1
            dona1 = new CocoRayado(dona1);

            ComplementoDona dona2 = new DonaVainilla(); //Se crea un pedido nuevo de una dona (Se permite elegir la dona base a gusto)

            //Se agregan los decoradores a la dona2
            dona2 = new FrutasConfitadas(dona2);

            //Se imprime en pantalla los dos pedidos de donas con su descripción 
            Console.WriteLine($"Pedido #1:  {dona1.Descripcion} ");
            Console.WriteLine($"Pedido #2:  {dona2.Descripcion} ");
            Console.ReadKey();
        }
    }
}
