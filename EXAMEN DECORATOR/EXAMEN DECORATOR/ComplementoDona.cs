﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN_DECORATOR
{
    public abstract class ComplementoDona 
    {
        public abstract string Descripcion { get; }
    }
}
